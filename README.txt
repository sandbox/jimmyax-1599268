YAHOO WEATHER FORECAST
----------------------

The module provides a block with current weather conditions and forecasts for
today and tomorrow.

Settings can be found at admin/settings/yahoo-weather-forecast. You must get
the city code from http://weather.yahoo.com. Look up the weather for a city and
you will see the code in the url.

CREDITS
-------
Developed at Alquimia Proyectos Digitales
http://al.quimia.net

Developed by:
 * Jacinto Capote <jacinto@al.quimia.net> http://drupal.org/user/348228
 * Pedro Lozano <pedro@al.quimia.net> http://drupal.org/user/123766
